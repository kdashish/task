const mongoose = require('mongoose');
module.exports = async () => {
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    };
    await mongoose.connect('mongodb://localhost:27017/school', options);
    console.log('Mongo connected');
};