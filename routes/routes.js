const express = require('express');
const router = express.Router();
const { userController } = require('../controllers/userController');
const { classController } = require('../controllers/classController');
const { subjectController } = require('../controllers/subjectController')
const { helper } = require('../helpers/helperMethods')

router.post('/signup', function (req, res) {
    userController.registerUser(req, res);
});
router.post('/login', function (req, res) {
    userController.loginUser(req, res);
});
router.post('/joinClass', helper.checkToken, function (req, res) {
    userController.joinClass(req, res);
});
router.post('/addClass', helper.checkToken, function (req, res) {
    classController.addClass(req, res);
});
router.delete('/deleteClass', helper.checkToken, function (req, res) {
    classController.deleteClass(req, res);
});
router.post('/addSubject', helper.checkToken, function (req, res) {
    subjectController.addSubject(req, res);
});
router.delete('/deleteSubject', helper.checkToken, function (req, res) {
    subjectController.deleteSubject(req, res);
});
router.post('/assignSubjectTeacher', helper.checkToken, function (req, res) {
    subjectController.assignSubjectTeacher(req, res);
});
router.post('/assignSubjectStudent', helper.checkToken, function (req, res) {
    subjectController.assignSubjectStudent(req, res);
});
router.delete('/deleteSubjectTeacher', helper.checkToken, function (req, res) {
    subjectController.deleteSubjectTeacher(req, res);
});
router.get('/fetchSubjects', helper.checkToken, function (req, res) {
    subjectController.fetchSubjects(req, res);
});
router.post('/assignClassTeacher', helper.checkToken, function (req, res) {
    classController.assignClassTeacher(req, res);
});
router.delete('/deleteClassTeacher', helper.checkToken, function (req, res) {
    classController.deleteClassTeacher(req, res);
});
router.get('/fetchClasses', helper.checkToken, function (req, res) {
    classController.fetchClasses(req, res);
});
router.get('/fetchTeachers', helper.checkToken, function (req, res) {
    userController.fetchTeachers(req, res);
});




//exporting router to use in our server.js

module.exports = router;