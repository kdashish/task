const { UserModel } = require('../models/userModel');
const { SubStudentModel } = require('../models/subStudentModel');
const { ClassStudentModel}=require('../models/classStudentModel');
let userServices = {};

/** -function to register a user */

userServices.regiter = async (payload) => {
    try {
        return await UserModel(payload).save();
    }
    catch (err) {
        return  err;
    }

};

/** -function to get user */

userServices.getUser = async (criteria) => {
    try {
        return await UserModel.findOne(criteria);
    }
    catch (err) {
        return err;
    }
};

/** -function to check whether student has already joined class or not */

userServices.checkJoinClass = async (criteria) => {
    try {
        return await ClassStudentModel.find(criteria).lean(); 
    }
    catch (err) {
        return err;
    }
};

/** -function to join a class for student */

userServices.joinClass = async (payload) => {
    try {
        return await ClassStudentModel(payload).save();
    }    
    catch (err) {
        return err;
    }
};

/** - function to fetch teachers */

userServices.fetchTeachers = async (criteria) => { 
    try {
        return await UserModel.find(criteria).lean();  
    }
    catch (err) {
        return err;
    }
};

/** -function to fetch teachers for students according to subjects */

userServices.fetchTeachersForStudents = async (criteria) => {

    try {
        return await SubStudentModel.aggregate([
            {
                "$lookup": {
                    from: 'subjectteachers',
                    localField: 'subjectID',
                    foreignField: 'subjectID',
                    as: 'teachers'
                }
            }
        ])
    }
    catch (err) {
        return err;
    }
};

module.exports = { userServices };