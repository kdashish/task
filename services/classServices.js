let ClassModel = require('../models/classModel');
let { ClassTeacherModel } = require('../models/classTeacherModel')
let classService = {};

/** -function to add a class */

classService.addClass = async (payload) => {
    try {
        return await ClassModel(payload).save();
    }
    catch (err) {
        return err;
    }
};

/** -function to delete a class */

classService.deleteClass = async (criteria) => {
    try {
        return await ClassModel.findOneAndDelete({ _id: criteria });
    }
    catch (err) {
        return err;
    }
};

/** -function to assign teacher a class */

classService.assignClassTeacher = async (payload) => {
    try {
        return await ClassTeacherModel(payload).save()
    }
    catch (err) {
        return err;
    }
};

/** -function to delete a class from teacher */

classService.deleteClassTeacher = async (criteria) => {
    try {
        return await ClassTeacherModel.findOneAndDelete(criteria);
    }
    catch (err) {
        return err;
    }
};

/** -function to fetch classes */

classService.fetch = async (criteria) => {
    try {
        return await ClassModel.find(criteria).lean();
    }
    catch (err) {
        return err;
    }
};

/** -function to fetch class assigned to teachers */

classService.fetchTeachersClass = async (criteria) => {
    try {
        return await ClassTeacherModel.find(criteria).lean();
    }
    catch (err) {
        return err;
    }
}
module.exports = { classService };