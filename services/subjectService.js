let { SubjectModel } = require('../models/subjectModel');
let { SubTeacherModel } = require('../models/subTeacherModel');
let { SubStudentModel } = require('../models/subStudentModel');
let subjectService = {};

/** -function to add a subject */

subjectService.addSubject = async (payload) => {
    try {
        return await SubjectModel(payload).save();
    }
    catch (err) {
        return err;
    }
};

/** -function to delete a subject */

subjectService.deleteSubject = async (payload) => {
    try {
        return await SubjectModel.findOneAndDelete({ _id: payload });
    }
    catch (err)
    {
        return err;
    }
};

/** -function to assign subject to teacher */

subjectService.assignSubjectTeacher = async (payload) => {
    try {
        return await SubTeacherModel(payload).save();
    }
    catch (err) {
        return err;
    }
};

/** -function to assign subject to student */

subjectService.assignSubjectStudent = async (payload) => {
    try {
        return await SubStudentModel(payload).save();
    }
    catch (err) {
        return err;
    }
};

/** -function to delete subject teacher */

subjectService.deleteSubjectTeacher = async (payload) => {
    try {
        return await SubTeacherModel.findOneAndDelete(payload);
    }
    catch (err) {
        return err;
    }
};

/** -function to fetch subjects */

subjectService.fetch = async (criteria) => {
    try {
        return await SubjectModel.find(criteria).lean();
    }
    catch (err) {
        return err;
    }
};

/** -function to fetch subjects for students */

subjectService.fetchStudentsSub = async (criteria) => {
    try {
        return await SubStudentModel.find(criteria).lean();
    }
    catch (err) {
        return err;
    }
};

/** -function to fetch subjects assigned to teachers */

subjectService.fetchTeachersSub = async (criteria) => {
    try { 
        return await SubTeacherModel.find(criteria).lean();
    }
    catch (err) {
        return err;
    }
};
module.exports = { subjectService };