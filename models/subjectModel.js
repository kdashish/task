"use strict";

const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;

const schema = new Schema({
    name: { type: String, required: true, unique: true},
    description: { type: String },
});

const SubjectModel = MONGOOSE.model('subject', schema);
module.exports = { SubjectModel };