"use strict";

const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;

const schema = new Schema({
    classID: { type: Schema.Types.ObjectId, ref: 'classes' },
    studentID: { type: Schema.Types.ObjectId, ref: 'users' },
});

const ClassStudentModel = MONGOOSE.model('classStudent', schema);
module.exports = { ClassStudentModel };