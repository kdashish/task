"use strict";

const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;

const schema = new Schema({
    class: { type: String, required: true,unique:true },
    description: { type: String },
});

const ClassModel = MONGOOSE.model('class', schema);
module.exports = ClassModel;