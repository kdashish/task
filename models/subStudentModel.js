"use strict";

const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;

const schema = new Schema({
    subjectID: { type: Schema.Types.ObjectId, ref: 'subjects' },
    studentID: { type: Schema.Types.ObjectId, ref: 'users' },
});

const SubStudentModel = MONGOOSE.model('subjectStudent', schema);
module.exports = { SubStudentModel };