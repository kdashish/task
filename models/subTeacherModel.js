"use strict";

const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;

const schema = new Schema({
    subjectID: { type: Schema.Types.ObjectId, ref: 'subjects'  },
    teacherID: { type: Schema.Types.ObjectId, ref: 'users'   },
});

const SubTeacherModel = MONGOOSE.model('subjectTeacher', schema);
module.exports = { SubTeacherModel };