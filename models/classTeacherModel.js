"use strict";

const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;

const schema = new Schema({
    classID: { type: Schema.Types.ObjectId, ref: 'classes' },
    teacherID: { type: Schema.Types.ObjectId, ref: 'users' },
});

const ClassTeacherModel = MONGOOSE.model('classTeacher', schema);
module.exports = { ClassTeacherModel };