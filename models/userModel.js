"use strict";

const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;

const schema = new Schema({
    name: { type: String, required: true },
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    role: {
        type: String,
        enum: ['Teacher', 'Student', 'Principle'],
        required: true
    },
    phone: { type: String, required: true },
});

const UserModel = MONGOOSE.model('user', schema);
module.exports = { UserModel };