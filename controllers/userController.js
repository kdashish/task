const { userServices } = require('../services/userServices');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { constants } = require('../utils/constants')
let userController = {};

/** -function to register user */

userController.registerUser = async (req, res) => {
    const payload = req.body;
    const checkUser = await userServices.getUser({ email: payload.email });
    if (checkUser) {
        res.status(400).json({ msg: "user already exist" });
    }
    payload.password = await bcrypt.hash(payload.password, 10);
    const user = await userServices.regiter(payload);
    res.status(200).json({ data: user });
};

/** -function to login user */

userController.loginUser = async (req, res) => {
    let payload = req.body;
    let user = await userServices.getUser({ email: payload.email });
    if (!user) {
        res.status(400).json({ msg: "user with that email doesnt exist" });
    }
    if (user.role !== payload.role) {
        res.status(401).json({ msg: "unauthorized access" });
    }
    const isMatched = await bcrypt.compareSync(payload.password, user.password);
    if (!isMatched) {
        res.status(400).json({ msg: "wrong credentials" });
    }
    let userData = {
        id: user._id,
        name: user.name,
        email: user.email,
        password: user.password,
        role: user.role,
        phone: user.phone
    };
    const token = await jwt.sign(userData, constants.SECRET.SECRET_KEY, { expiresIn: '120ms' });
    res.status(200).send({ data: userData, token: token });
};

/** -function to join class for student */

userController.joinClass = async (req, res) => {

    let payload = req.body;
    let data = jwt.decode(req.token);
    console.log(data);
    
    let checkJoinClass = await userServices.checkJoinClass({ studentID: data.id })
    if (checkJoinClass) {
        res.status(400).send({ msg: `already joined,cant join any other class` });
    }
    await userServices.joinClass({ classID: payload.classID, studentID: data.id }
    );
    res.status(200).send({ msg: `class joined` });

};

/** -function to fetch teachers */

userController.fetchTeachers = async (req, res) => {
    let data = jwt.decode(req.token);
    let teachers = {};
    console.log(data);
    if (data.role === "Principle" || data.role === "Teacher" ) {
        teachers = await userServices.fetchTeachers({ role: "Teacher" });
    }
    if (data.role === "Student") {
        teachers = await userServices.fetchTeachersForStudents();
    }
    res.status(200).send({ data: teachers });
};
module.exports = { userController };