const { subjectService } = require('../services/subjectService');
let jwt = require('jsonwebtoken');
let subjectController = {};

/** -function to add a subject */

subjectController.addSubject = async (req, res) => {
    let payload = req.body;
    let userData = jwt.decode(req.token);
    if (userData.role !== "Principle") {
        res.status(401).send({ msg: `Not authorized to add a new subject` });
    }
    let newSubject = await subjectService.addSubject(payload);
    res.status(200).send({ data: newSubject });
};

/** -function to delete a subject */

subjectController.deleteSubject = async (req, res) => {
    let payload = req.body;
    let userData = jwt.decode(req.token);
    if (userData.role !== "Principle") {
        res.status(401).send({ msg: `Not authorized to delete subject` });
    }

    let deletedSubject = await subjectService.deleteSubject(payload.subjectID);
    res.status(200).send({ data: deletedSubject });
};

/** -function to assign a subject to teacher */

subjectController.assignSubjectTeacher = async (req, res) => {
    let payload = req.body;
    let userData = jwt.decode(req.token);
    if (userData.role !== "Principle") {
        res.status(401).send({ msg: `Not authorized to assign subject` });
    }
    let assignedSubTeacher = await subjectService.assignSubjectTeacher(payload);
    res.status(200).send({ data: assignedSubTeacher });
};

/** -function to assign subject to student */

subjectController.assignSubjectStudent = async (req, res) => {
    let payload = req.body;
    let userData = jwt.decode(req.token);
    if (userData.role !== "Principle") {
        res.status(401).send({ msg: `Not authorized to assign subject` });
    }
    let assignedSubStudent = await subjectService.assignSubjectStudent(payload);
    res.status(200).send({ data: assignedSubStudent });
};

/** -fucntion to delete subject from teacher */

subjectController.deleteSubjectTeacher = async (req, res) => {
    let payload = req.body;
    let userData = jwt.decode(req.token);
    if (userData.role !== "Principle") {
        res.status(401).send({ msg: `Not authorized to delete subject from teacher` });
    }
    let deletedSubTeacher = await subjectService.deleteSubjectTeacher(payload);
    res.status(200).send({ data: deletedSubTeacher });
};

/** -function to fetch subjects */

subjectController.fetchSubjects = async (req, res) => {
    let subjects = {};
    let userData = jwt.decode(req.token);
    console.log(userData);
    
    if (userData.role === "Principle") {
        subjects = await subjectService.fetch({});
    }
    if (userData.role === "Student") {
        subjects = await subjectService.fetchStudentsSub({studentID:userData.id});
    }
    if (userData.role === "Teacher") {
        console.log("here");
        
        subjects = await subjectService.fetchTeachersSub({ teacherID: userData.id });
    }
    res.status(200).send({ data: subjects });
};

module.exports = { subjectController };