const { classService } = require('../services/classServices');
let jwt = require('jsonwebtoken');
let classController = {};

/** -function to add a class */

classController.addClass = async (req, res) => {
    let payload = req.body;
    let userData = jwt.decode(req.token);
    if (userData.role !== "Principle") {
        res.status(401).send({ msg: `Not authorized to add a new class` });
    }
    let newClass = await classService.addClass(payload);
    res.status(200).send({ data: newClass });
};

/** -function to delete a class */

classController.deleteClass = async (req, res) => {
    let payload = req.body;
    let userData = jwt.decode(req.token);
    if (userData.role !== "Principle") {
        res.status(401).send({ msg: `Not authorized to delete class` });
    }

    let deletedClass = await classService.deleteClass(payload.classID);
    res.status(200).send({ data: deletedClass });
};

/** -function to assign a class to teacehr */

classController.assignClassTeacher = async (req, res) => {
    let payload = req.body;
    let userData = jwt.decode(req.token);
    if (userData.role !== "Principle") {
        res.status(401).send({ msg: `Not authorized to assign class to teacher` });
    }
    let classTeacher = await classService.assignClassTeacher(payload);
    res.status(200).send({ data: classTeacher });
};

/** -function to delete class from teacher */

classController.deleteClassTeacher = async (req, res) => {
    let payload = req.body;
    let userData = jwt.decode(req.token);
    if (userData.role !== "Principle") {
        res.status(401).send({ msg: `Not authorized to delete class from teacher` });
    }
    let deleteClassTeacher = await classService.deleteClassTeacher(payload);
    res.status(200).send({ data: deleteClassTeacher });
};

/** -function to fetch classes */

classController.fetchClasses = async (req, res) => {
    let classes = {};
    let userData = jwt.decode(req.token);
    console.log(userData);

    if (userData.role === "Principle") {
        classes = await classService.fetch({});
    }
    if (userData.role === "Teacher") {
        classes = await classService.fetchTeachersClass({ teacherID: userData.id });
    }
    res.status(200).send({ data: classes });
}

module.exports = { classController };