const express = require('express');
const app = express();
const PORT = 7000;
const routes = require('./routes/routes');

//both index.js and things.js should be in same directory
require('./mongo')();
app.use(require("body-parser").json());
app.use(require("body-parser").urlencoded({ extended: true }));
app.use('/', routes);

app.listen(PORT, () => console.log(`app listening at http://localhost:${PORT}`))